import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.Popups 1.0
import ShadowSocks 1.0
import "OfflineStorage.js" as ShadowSocksStorage

Page {
    id: mainPage;
    title: i18n.tr("ShadowSocks-Qml")

    head.actions: [
        Action {
            id: historyAction;
            iconName: "contextual-menu"
            text: i18n.tr("History")
            visible: ssClient.status !== ShadowSocksClient.Running;
            onTriggered: {
                showHistory();
            }
        },
        Action {
            id: settingsAction;
            iconName: "settings"
            text: i18n.tr("Settings")
            onTriggered: {
                showSettings();
            }
        }
    ]

    state: "Disconnected"

    function connect(serverIp, serverPort, localAddress,
                     localPort, method, password) {
        ssClient.serverAddress = serverIp;
        ssClient.serverPort = serverPort;
        ssClient.localAddress = localAddress;
        ssClient.localPort = localPort;
        ssClient.method = method;
        ssClient.password = password;
        monitor.start()


        if (!ssClient.start()) {
            PopupUtils.open(prompt, null);
            monitor.stop();
        } else {
            mainPage.state = "Connection";
            ShadowSocksStorage.updateHistory(ssClient);
        }
    }

    function showHistory() {
        var serverAddress = serverAddr.text;
        pageStack.push(Qt.resolvedUrl("./HistoryPage.qml"),
                       {remoteAddress:serverAddress, mainPage: mainPage});
    }

    function showSettings() {
        pageStack.push(Qt.resolvedUrl("./SettingsPage.qml"));
    }

    Column {
        id: column;
        spacing: units.gu(0.5)
        anchors {
            margins: units.gu(2)
            fill: parent
        }
        clip: true;
        transform: Scale {
            id: yTransform;
            origin.x: column.width / 2.0;
            origin.y: 0;
        }

        Label {
            text: i18n.tr("Server IP")
        }

        TextField {
            id: serverAddr;
            width: parent.width;
            validator: RegExpValidator {
                regExp:  /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])(\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])){3}$/
            }
            KeyNavigation.tab: serverPort;
        }

        Divider{}

        Label {
            text: i18n.tr("Server Port")
        }

        TextField {
            id: serverPort;
            width: parent.width;
            validator:IntValidator {
                bottom: 1
                top: 65535
            }
            KeyNavigation.tab: password;
        }

        Divider{}

        Label {
            text: i18n.tr("Password")
        }

        TextField {
            id: password;
            width: parent.width;
            echoMode: TextInput.Password
            validator: RegExpValidator {
                regExp: /^[^\s]+$/
            }
            KeyNavigation.tab: localAddr;
        }

        Divider{}

        //https://github.com/shadowsocks/shadowsocks/wiki/Encryption
        //Deprecated Ciphers [rc4, des-cfb, table, salsa20-ctr]
        OptionSelector {
            id: methodSelector;
            text: i18n.tr("Method")
            model: [ i18n.tr("AES-128-CFB"),
                i18n.tr("AES-192-CFB"),
                i18n.tr("AES-256-CFB"),
                i18n.tr("BF-CFB"),
                i18n.tr("Camellia-128-CFB"),
                i18n.tr("Camellia-192-CFB"),
                i18n.tr("Camellia-256-CFB"),
                i18n.tr("CAST5-CFB"),
                i18n.tr("ChaCha20"),
                i18n.tr("IDEA-CFB"),
                i18n.tr("RC2-CFB"),
                i18n.tr("RC4-MD5"),
                i18n.tr("SEED-CFB")
            ]
            containerHeight: itemHeight * 4
            selectedIndex: 0;
        }

        Divider{}

        Label {
            text: i18n.tr("Local Address")
        }

        TextField {
            id: localAddr;
            text: "127.0.0.1";
            width: parent.width;
            validator: RegExpValidator {
                regExp:  /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])(\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])){3}$/
            }
            KeyNavigation.tab: localPort;
        }


        Divider{}

        Label {
            text: i18n.tr("Local Port")
        }

        TextField {
            id: localPort;
            width: parent.width;
            text: "1080"
            validator: IntValidator {
                bottom: 1
                top: 65535
            }
            KeyNavigation.tab: connectBtn;
        }
    }

    Monitor {
        id: monitor;
        anchors.left: column.left;
        anchors.right: column.right;
        height: parent.height - connectBtn.height;
    }


    ShadowSocksClient {
        id: ssClient;
        onLogInfo: {
            monitor.appendInfo(info);
        }
        onBytesSentChanged: {
            monitor.totalBytesSent = totalBytes;
        }

        onBytesReceivedChanged: {
            monitor.totalBytesReceived = totalBytes;
        }
    }

    Button {
        id: connectBtn;
        width: parent.width - units.gu(3);
        anchors.horizontalCenter: parent.horizontalCenter;
        anchors.bottom:parent.bottom;
        anchors.bottomMargin: units.gu(2);

        onClicked: {
            if (mainPage.state === "Connection") {
                mainPage.state = "Disconnected";
                ssClient.stop();
                monitor.stop();
            } else {

                var sIp = serverAddr.text;
                var sPort = serverPort.text;
                var passwd = password.text;
                var lIp = localAddr.text;
                var lPort = localPort.text;
                var method = methodSelector.model[methodSelector.selectedIndex];

                if (sIp.length === 0) {
                    PopupUtils.open(tips, serverAddr,
                                    {msg: i18n.tr("Server IP should not be empty")});
                } else if (lIp.length === 0) {
                    PopupUtils.open(tips, localAddr,
                                    {msg: i18n.tr("Local IP should not be empty")});
                } else if (sPort.length === 0) {
                    PopupUtils.open(tips, serverPort,
                                    {msg: i18n.tr("Server Port  should not be empty")});
                } else if (passwd.length === 0) {
                    PopupUtils.open(tips, password,
                                    {msg: i18n.tr("Password should not be empty")});
                } else if (lPort.length === 0) {
                    PopupUtils.open(tips, localPort,
                                    {msg: i18n.tr("Local Port should not be empty")})
                } else if (sIp.match(/^(([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))\.){3}([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))$/)
                           == null) {
                    PopupUtils.open(tips, serverAddr,
                                    {msg: i18n.tr("Invalid IP Address")})
                } else if (lIp.match(/^(([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))\.){3}([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))$/)
                           == null) {
                    PopupUtils.open(tips, localAddr,
                                    {msg: i18n.tr("Invalid IP Address")})
                } else {
                    connect(sIp, sPort, lIp, lPort, method, passwd);
                }
            }
        }
    }

    states: [
        State {
            name: "Connection"
            PropertyChanges { target: yTransform; yScale: 0.0 }
            PropertyChanges { target: column; opacity: 0.0}
            PropertyChanges { target: connectBtn; text: i18n.tr("Disconnect")}
            PropertyChanges { target: monitor; opacity: 1.0}
            AnchorChanges { target:  monitor; anchors.bottom: undefined; anchors.top: mainPage.top}
        },
        State {
            name: "Disconnected"
            PropertyChanges { target: yTransform; yScale: 1.0 }
            PropertyChanges { target: column; opacity: 1.0}
            PropertyChanges { target: connectBtn; text: i18n.tr("Connect")}
            PropertyChanges { target: monitor; opacity: 0.0}
            AnchorChanges { target:  monitor; anchors.bottom: mainPage.bottom; anchors.top: undefined}
        }
    ]

    transitions: [
        Transition {
            from: "Connection"
            to: "Disconnected"
            reversible: true;
            PropertyAnimation {
                target: yTransform;
                properties: "yScale";
                duration: 275;
                easing.type:  Easing.InQuad;
            }
            PropertyAnimation {
                targets: [column, monitor]
                properties: "opacity";
                duration: 275;
            }
            AnchorAnimation {
                duration: 275;
            }
        }
    ]

    Component {
        id: tips

        Popover {
            id: popover;
            autoClose: true;
            property alias msg:  msgLabel.text;
            contentWidth: msgLabel.contentWidth + units.gu(2.0);

            Label {
                id: msgLabel;
                height: units.gu(4)
                anchors.horizontalCenter: parent.horizontalCenter;
            }
        }
    }

    Component {
        id: prompt;

        Dialog {
            id: dlg;
            title: i18n.tr("Ooops...")
            text: i18n.tr("ShadowSockClient failed to start, Pls try it again later on.")

            Button {
                anchors.horizontalCenter: parent.horizontalCenter;
                width: parent.width /2;
                text: i18n.tr("Confirm");
                onClicked: PopupUtils.close(dlg);
            }
        }
    }

    Component.onCompleted: {
        ShadowSocksStorage.getLastHistory(function (lastItem){
            serverAddr.text = lastItem.serverAddress;
            serverPort.text = lastItem.serverPort;
            localAddr.text  = lastItem.localAddress;
            localPort.text  = lastItem.localPort;
            password.text = lastItem.password;

            for (var i = 0; i < methodSelector.model.length; i++) {
                if (methodSelector.model[i] === lastItem.method) {
                    methodSelector.selectedIndex = i;
                    break;
                }
            }
        });
    }
}
