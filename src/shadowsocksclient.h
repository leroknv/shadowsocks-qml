#ifndef SHADOWSOCKSCLIENT_H
#define SHADOWSOCKSCLIENT_H

#include <QQuickItem>
#include <qtshadowsocks/QtShadowsocks>

class ShadowSocksClient : public QQuickItem
{
    Q_OBJECT

    Q_ENUMS(Status)
    Q_PROPERTY(Status status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(int localPort READ localPort WRITE setLocalPort NOTIFY localPortChanged)
    Q_PROPERTY(QString localAddress READ localAddress WRITE setLocalAddress NOTIFY localAddressChanged)
    Q_PROPERTY(int serverPort READ serverPort WRITE setServerPort NOTIFY serverPortChanged)
    Q_PROPERTY(QString serverAddress READ serverAddress WRITE setServerAddress NOTIFY serverAddressChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(QString method READ method WRITE setMethod NOTIFY methodChanged)
    Q_PROPERTY(int timeout READ timeout WRITE setTimeout NOTIFY timeoutChanged)
public:
    enum Status{
        Running = 0,
        StandBy
    };

    explicit ShadowSocksClient(QQuickItem *parent = 0);

    Q_INVOKABLE bool start();

    Q_INVOKABLE void stop();

    Status status() const {
        return mStatus;
    }
    void setStatus(ShadowSocksClient::Status status);

    int localPort() const {
        return mLocalPort;
    }
    void setLocalPort(int port);

    QString localAddress() const {
        return mLocalAddress;
    }
    void setLocalAddress(const QString& address);

    int serverPort() const {
        return mServerPort;
    }
    void setServerPort(int port);

    QString serverAddress() const {
        return mServerAddr;
    }
    void setServerAddress(const QString& address);

    QString password() const {
        return mPassword;
    }
    void setPassword(const QString& password);

    QString method() const {
        return mMethod;
    }
    void setMethod(const QString& method);

    int timeout() const {
        return mTimeout;
    }
    void setTimeout(int timeout);

signals:
    void statusChanged();

    void localAddressChanged();

    void localPortChanged();

    void methodChanged();

    void serverPortChanged();

    void serverAddressChanged();

    void passwordChanged();

    void timeoutChanged();

    void logInfo(const QString& info);

    void bytesReceivedChanged(const qint64 &totalBytes);

    void bytesSentChanged(const qint64 &totalBytes);

    void newBytesSent(const qint64 & newBytes);

    void newBytesReceived(const qint64 &newBytes);

private:
    int     mLocalPort;
    QString mLocalAddress;
    int     mServerPort;
    QString mServerAddr;
    QString mPassword;
    QString mMethod;
    int     mTimeout;

    Status  mStatus;
    QSS::Controller *mController;
};

#endif // SHADOWSOCKSCLIENT_H
