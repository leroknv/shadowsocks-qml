import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 0.1 as ListItem
import "OfflineStorage.js" as ShadowSocksStorage

Page {
    title: i18n.tr("History")

    property var    mainPage;
    property string remoteAddress;

    head.backAction: Action {
        iconName: "back"
        onTriggered: {
            pageStack.pop();
        }
    }


    Label {
        visible:historyModel.count == 0 ? true: false;
        text: i18n.tr("No record yet")
        fontSize: "x-large";
        anchors.centerIn: parent;
    }

    Column {
        id: column;
        spacing: units.gu(0.5)
        anchors {
            margins: units.gu(2)
            fill: parent
        }

        Repeater {
            id: repeater;
            model: historyModel;
            delegate: ListItem.Subtitled {
                text: serverAddress;
                subText: new Date(date);

                CheckBox {
                    id:checkBox;
                    anchors.right: parent.right;
                    anchors.verticalCenter: parent.verticalCenter;
                    checked: serverAddress == remoteAddress? true: false;
                    enabled: false;
                }

                onClicked: {
                    mainPage.connect(serverAddress, serverPort, localAddress,
                                     localPort, method, password);

                    pageStack.pop();
                }
            }
        }

        ListModel {
            id: historyModel;
        }
    }

    Component.onCompleted: {
        ShadowSocksStorage.getHistoryList(function (historyList){
            for (var i = 0; i < historyList.length; i++) {
                historyModel.append({"serverAddress": historyList[i].serverAddress,
                                     "serverPort"   : historyList[i].serverPort,
                                     "localAddress" : historyList[i].localAddress,
                                     "localPort"    : historyList[i].localPort,
                                     "password"     : historyList[i].password,
                                     "method"       : historyList[i].method,
                                     "date"         : historyList[i].date});
            }
        });
    }
}
