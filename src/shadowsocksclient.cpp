#include "shadowsocksclient.h"
#include <QDesktopServices>
ShadowSocksClient::ShadowSocksClient(QQuickItem *parent) :
    QQuickItem(parent)
  , mTimeout(300)
  , mStatus(ShadowSocksClient::StandBy)
  , mController(NULL)
{
    this->setFlag(QQuickItem::ItemHasContents, false);
}

bool ShadowSocksClient::start()
{
    if (mController) {
        mController->deleteLater();
    }
    mController = new QSS::Controller(true, this);
    connect(mController, &QSS::Controller::error, this, &ShadowSocksClient::logInfo);
    connect(mController, &QSS::Controller::info, this, &ShadowSocksClient::logInfo);
    connect(mController, &QSS::Controller::bytesSentChanged, this, &ShadowSocksClient::bytesSentChanged);
    connect(mController, &QSS::Controller::bytesReceivedChanged, this, &ShadowSocksClient::bytesReceivedChanged);
    connect(mController, &QSS::Controller::newBytesSent, this,&ShadowSocksClient::newBytesSent);
    connect(mController, &QSS::Controller::newBytesReceived, this, &ShadowSocksClient::newBytesReceived);

    QSS::Profile profile;
    profile.local_address = mLocalAddress;
    profile.local_port = mLocalPort;
    profile.method = mMethod;
    profile.password = mPassword;
    profile.server = mServerAddr;
    profile.server_port = mServerPort;
    profile.timeout = mTimeout;

    mController->setup(profile);
    bool ret = mController->start();

    ShadowSocksClient::Status status = ret == true ?
                ShadowSocksClient::Running:ShadowSocksClient::StandBy;
    setStatus(status);

    return ret;
}

void ShadowSocksClient::stop()
{
    setStatus(ShadowSocksClient::StandBy);

    mController->stop();
}

void ShadowSocksClient::setStatus(ShadowSocksClient::Status status)
{
    if (mStatus != status){
        mStatus = status;

        emit statusChanged();
    }
}

void ShadowSocksClient::setLocalPort(int port)
{
    if (mLocalPort != port) {
        mLocalPort = port;

        emit localPortChanged();
    }
}

void ShadowSocksClient::setLocalAddress(const QString &address)
{
    if (mLocalAddress != address){
        mLocalAddress = address;

        emit localAddressChanged();
    }
}

void ShadowSocksClient::setMethod(const QString &method)
{
    if (mMethod != method) {
        mMethod = method;

        emit methodChanged();
    }
}

void ShadowSocksClient::setServerPort(int port)
{
    if (mServerPort != port) {
        mServerPort = port;

        emit serverPortChanged();
    }
}

void ShadowSocksClient::setServerAddress(const QString &address)
{
    if (mServerAddr != address) {
        mServerAddr = address;

        emit serverAddressChanged();
    }
}

void ShadowSocksClient::setPassword(const QString &password)
{
    if (mPassword != password) {
        mPassword = password;

        emit passwordChanged();
    }
}

void ShadowSocksClient::setTimeout(int timeout)
{
    if (mTimeout != timeout) {
        mTimeout = timeout;

        emit timeoutChanged();
    }
}
