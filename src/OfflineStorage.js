.pragma library
.import QtQuick.LocalStorage 2.0 as Sql

function getLastHistory(callback) {
    var db = Sql.LocalStorage.openDatabaseSync(
                "ShadowSocks-Qml", "1.0", "shadowsocks-qml offline storage", 100);

    var lastItem = {};
    db.transaction (function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS history (
                        serverAddress TEXT UNIQUE,
                        serverPort  NUMBER,
                        localAddress TEXT,
                        localPort   NUMBER,
                        method      TEXT,
                        password    TEXT,
                        date        NUMBER)");
        var rs = tx.executeSql("SELECT * FROM history order by date desc");

        if (rs.rows.length > 0) {
            lastItem = {
                serverAddress: rs.rows.item(0).serverAddress,
                serverPort  : rs.rows.item(0).serverPort,
                localAddress: rs.rows.item(0).localAddress,
                localPort   : rs.rows.item(0).localPort,
                method      : rs.rows.item(0).method,
                password    : rs.rows.item(0).password
            };

            callback(lastItem);
        }
    })
}

function getHistoryList(callback) {
    var db = Sql.LocalStorage.openDatabaseSync(
                "ShadowSocks-Qml", "1.0", "shadowsocks-qml offline storage", 100);

    var list = [];
    db.transaction (function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS history (
                        serverAddress TEXT UNIQUE,
                        serverPort  NUMBER,
                        localAddress TEXT,
                        localPort   NUMBER,
                        method      TEXT,
                        password    TEXT,
                        date        NUMBER)");
        var rs = tx.executeSql("SELECT * FROM history order by date desc");

        var item = {};
        for (var i = 0; i < rs.rows.length; i++) {
            item = {
                serverAddress: rs.rows.item(i).serverAddress,
                serverPort  : rs.rows.item(i).serverPort,
                localAddress: rs.rows.item(i).localAddress,
                localPort   : rs.rows.item(i).localPort,
                method      : rs.rows.item(i).method,
                password    : rs.rows.item(i).password,
                date        : rs.rows.item(i).date
            };

            list.push(item);
        }

        callback(list);
    })
}

function updateHistory(ssClient) {
    var db = Sql.LocalStorage.openDatabaseSync(
                "ShadowSocks-Qml", "1.0", "shadowsocks-qml offline storage", 100);

    db.transaction (function (tx) {
        tx.executeSql("INSERT OR REPLACE INTO history VALUES(?, ?, ?, ?, ?, ?, ?)",
                      [ssClient.serverAddress,
                       ssClient.serverPort,
                       ssClient.localAddress,
                       ssClient.localPort,
                       ssClient.method,
                       ssClient.password,
                       new Date().getTime()]);
    })
}

function getSettings(callback) {
    var db = Sql.LocalStorage.openDatabaseSync(
                "ShadowSocks-Qml", "1.0", "shadowsocks-qml offline storage", 100);

    db.transaction (function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS settings (key TEXT UNIQUE, value TEXT)");
        var rs = tx.executeSql("SELECT * FROM settings");

        var item = {};
        for (var i = 0; i < rs.rows.length; i++) {
            item[rs.rows.item(i).key] = rs.rows.item().value;
        }

        callback(item);
    })
}

function updateSettings(key, value) {
    var db = Sql.LocalStorage.openDatabaseSync(
                "ShadowSocks-Qml", "1.0", "shadowsocks-qml offline storage", 100);

    db.transaction (function (tx){
        tx.executeSql("INSERT OR REPLACE INTO settings VALUES(?, ?)",
                      [key, value]);
    })
}

