TEMPLATE = app
TARGET = shadowssock-qml

load(ubuntu-click)

QT += qml quick network

CONFIG += c++11

SOURCES += main.cpp \
    shadowsocksclient.cpp

RESOURCES += shadowsocks-qml.qrc

OTHER_FILES += shadowsocks-qml.apparmor \
               shadowsocks-qml.desktop \
               shadowsocks-qml.png

#specify where the config files are installed to
config_files.path = /shadowsock-qml
config_files.files += $${OTHER_FILES}
message($$config_files.files)
INSTALLS+=config_files

# Default rules for deployment.
target.path = $${UBUNTU_CLICK_BINARY_PATH}
INSTALLS+=target

HEADERS += \
    shadowsocksclient.h

LIBS += -lQtShadowsocks -lbotan-1.10
