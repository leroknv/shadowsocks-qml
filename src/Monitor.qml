import QtQuick 2.3
import Ubuntu.Components 1.1
import "OfflineStorage.js" as ShadowSocksStorage

Item {
    id: monitor;

    property real totalBytesSent: 0;
    property real totalBytesReceived: 0;

    function stop() {
        timer.stop();
    }

    function appendInfo(logInfo) {
        logArea.text = logArea.text +logInfo + "\n";
    }

    function start() {
        logArea.text = "";
        privObj.counter = 0;
        privObj.oldBytesReceived = privObj.oldBytesSent = 0;
        totalBytesSent = totalBytesReceived = 0;

        timer.start();
    }

    Repeater {
        id: txRepeater;
        model: canvas.width;
        Item { id: txObj; property real bytes }
    }

    Repeater {
        id: rxRepeater;
        model: canvas.width;
        Item { id: rxObj; property real bytes }
    }

    Timer {
        id: timer;
        repeat: true;
        interval: 1000 / privObj.frames;
        triggeredOnStart: true;

        onTriggered: {
            privObj.counter++;

            privObj.currentRx = (totalBytesReceived - privObj.oldBytesReceived) / ( privObj.counter / privObj.frames) / 1024 ;
            privObj.currentTx = (totalBytesSent - privObj.oldBytesSent) / ( privObj.counter / privObj.frames)  / 1024 ;

            if (privObj.counter % privObj.frames == 0) {
                privObj.oldBytesReceived  = totalBytesReceived;
                privObj.oldBytesSent = totalBytesSent;

                privObj.counter = 0;
            }

            for (var i = rxRepeater.count-1; i >= 1; i--) {
                rxRepeater.itemAt(i).bytes = rxRepeater.itemAt(i - 1).bytes;
            }

            rxRepeater.itemAt(0).bytes= privObj.currentRx;


            for (var i = txRepeater.count-1; i >= 1; i--) {
                txRepeater.itemAt(i).bytes = txRepeater.itemAt(i - 1).bytes;
            }

            txRepeater.itemAt(0).bytes= privObj.currentTx;


            if (privObj.currentRx > (2 << privObj.scalefactor) || privObj.currentTx > ( 2 << privObj.scalefactor)) {
                privObj.scalefactor++;
            } else if ( privObj.scalefactor > privObj.minfactor){
                var i = 0;
                var maxValue = 2 << (privObj.scalefactor - 1);
                for (; i < rxRepeater.count; i++) {
                    if (rxRepeater.itemAt(i).bytes > maxValue)
                        break;
                }
                if (i === rxRepeater.count) {
                    i = 0;
                    for (; i < txRepeater.count; i++) {
                        if (txRepeater.itemAt(i).bytes > maxValue)
                            break;
                    }

                    if (i === txRepeater.count) {
                        privObj.scalefactor--;
                    }
                }
            }

            canvas.requestPaint();
        }
    }

    Canvas {
        id: canvas;

        width: parent.width;
        height: parent.height / 2.5;
        anchors.top: parent.top;
        anchors.topMargin: units.gu(1)

        function drawTraffic(ctx, repeater, pen, brush) {
            ctx.strokeStyle = pen;
            ctx.fillStyle = brush;
            ctx.beginPath();
            ctx.moveTo(0, canvas.height);

            //calculate
            var baseValue = (2 << privObj.scalefactor);
            for (var i = 0; i < canvas.width; i++) {
                ctx.lineTo(i, (1 - repeater.itemAt(i).bytes / baseValue) * canvas.height);
            }
            ctx.lineTo(canvas.width, canvas.height);
            ctx.lineTo(0, canvas.height);
            ctx.closePath();

            ctx.fill();
            ctx.stroke();
        }

        onPaint: {
            var ctx = canvas.getContext('2d');

            ctx.save();

            ctx.clearRect(0,0,canvas.width,canvas.height);

            //RX path
            drawTraffic(ctx, rxRepeater, "cyan", "blue");
            //TX path
            drawTraffic(ctx, txRepeater, "orange", "red");

            ctx.strokeStyle = "blue";
            ctx.textAlign = "end";
            ctx.textBaseline = "top";
            ctx.font = "12px sans-serif";

            var rxTotal = (totalBytesReceived / 1024 / 1024).toFixed(2) ;
            var txTotal = (totalBytesSent / 1024 / 1024).toFixed(2);
            var rxKbs = privObj.currentRx.toFixed(1) +i18n.tr("KB/s");
            var txKbs = privObj.currentTx.toFixed(1) +i18n.tr("KB/s");
            ctx.strokeText(i18n.tr("Rx: ") + rxTotal + i18n.tr("MB"),canvas.width,units.gu(0));
            ctx.strokeText(i18n.tr("Tx: ") + txTotal + i18n.tr("MB"),canvas.width,units.gu(1.5));
            ctx.strokeText(rxKbs + " | " +txKbs,canvas.width,units.gu(3));

            ctx.restore();
        }
    }


    //Ubuntu TextArea doesn't support append info to autoscroll content
    //A workaround doesn't work out.
    //However offical Qml TextArea works fine.
    UbuntuShape {
        id: frameBorder;
        radius: "medium"
        color: "black"
        anchors.top: canvas.bottom;
        anchors.topMargin: units.gu(2);
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: units.gu(3);
        width: parent.width;

        Flickable {
            id: flicker
            objectName: "input_scroller"
            anchors {
                fill: parent
                margins: units.gu(1)
            }
            clip: true
            contentWidth: logArea.paintedWidth;
            contentHeight: logArea.paintedHeight;
            boundsBehavior: Flickable.StopAtBounds
            onContentHeightChanged: {
                if (contentHeight > flicker.height) {
                    contentY = contentHeight - flicker.height;
                }
            }

            TextEdit {
                id: logArea;
                width: frameBorder.width;
                height: frameBorder.height;
                readOnly: true;
                wrapMode: TextEdit.WrapAtWordBoundaryOrAnywhere
                font.pixelSize: FontUtils.sizeToPixels("small")
                color: "white"
            }
        }
    }

    QtObject {
        id: privObj;
        readonly property int frames: 2;
        readonly property int minfactor: 9;
        property int scalefactor: minfactor;

        property int counter: 0;
        property real oldBytesReceived: 0
        property real oldBytesSent: 0
        property real currentRx:0;
        property real currentTx:0;
    }
}
