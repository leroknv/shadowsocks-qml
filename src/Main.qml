import QtQuick 2.3
import Ubuntu.Components 1.1

MainView {
    objectName: "mainView"
    applicationName: "shadowsocks-qml"
    useDeprecatedToolbar: false;

    width: units.gu(45)
    height: units.gu(70)

    property var settings: {
        "reset_network_traffic" : false,
    }

    PageStack {
        id: pageStack
        anchors.fill: parent;
    }

    Component.onCompleted: {
        pageStack.push(Qt.resolvedUrl("MainPage.qml"));
    }
}

