import QtQuick 2.3
import Ubuntu.Components 1.1
import "OfflineStorage.js" as ShadowSocksStorage

Page {
    title: i18n.tr("Settings")

    property alias trafficReset : trafficSwitch.checked;

    head.backAction: Action {
        iconName: "back"
        onTriggered: {
            pageStack.pop();
        }
    }

    Column {
        id: column;
        spacing: units.gu(0.5)
        anchors {
            margins: units.gu(2)
            fill: parent
        }

        Item {
            width: parent.width;
            height: Math.max(label.height, trafficSwitch.height)

            Label {
                id: label
                text: i18n.tr("Reset network traffic");
                anchors.left: parent.left;
                anchors.verticalCenter: parent.verticalCenter
                elide: Text.ElideRight
                font.weight: Font.Light
            }

            Switch {
                id: trafficSwitch;
                anchors.right: parent.right;
                anchors.verticalCenter: parent.verticalCenter
                onTriggered: {
                    settings["reset_network_traffic"] = checked;
                    ShadowSocksStorage.updateSettings("reset_network_traffic",
                                                      settings["reset_network_traffic"]);
                }
            }
        }
    }

    Component.onCompleted: {
        ShadowSocksStorage.getSettings(function (settingItems){
            for (var settingsKey in settingItems){
                settings[settingsKey] = settingItems[settingsKey];
            }
        });

        trafficSwitch.checked = parseInt(settings["reset_network_traffic"]);
    }
}
