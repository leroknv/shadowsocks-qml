ShadowSocks-Qml
================

Introduction
------------
ShadowSocks client for Ubuntu touch based on QtShadowsocks

Compile 
--------------
- Install dependences
	sudo apt-get install libbotan1.10-dev 

- Checkout QtShadowsocks
    git clone https://github.com/librehat/libQtShadowsocks.git
	cd libQtShadowsocks/ && qmake
	make && make install

- Checkout Shadowsocks-qml 
    qmake && make

Then, enjoy.

Note
--------------
For ubuntu touch, By far no way to set system-wide proxy server in ubuntu-touch


License
-------
Copyright (C) 2014-2015 Gary.Wzl <Gary.Wang@canonical.com>

This project is licensed under version 3 of the GNU Lesser General Public License.
